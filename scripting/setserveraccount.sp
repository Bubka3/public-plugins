#include <sourcemod>
#include <steamworks>
#pragma semicolon 1
#define Plugin_Version "1.1.1"

public Plugin:myinfo =
{
	name = "Set Game Server Account",
	author = "Bubka3",
	description = "A simple plugin based on the steamworks extension to login to a game server account.",
	version = Plugin_Version,
	url = "http://www.bubka3.com/"
};

public OnPluginStart()
{
	if(GetCommandFlags("sv_setsteamaccount") != INVALID_FCVAR_FLAGS)
		SetFailState("You do not need the Set Game Server Account plugin! This convar is already built into your game.");

	CreateConVar("sm_steamaccount_version", Plugin_Version, "This is the version of the framework the server is running.", FCVAR_PLUGIN|FCVAR_NOTIFY|FCVAR_SPONLY);
}

public SteamWorks_TokenRequested(String:sToken[], maxlen)
{
	new String:FilePath[256];
	BuildPath(Path_SM, FilePath, 256, "configs/gameserveraccount_token.txt");
	
	new Handle:hFileToken = OpenFile(FilePath, "r");

	if(hFileToken == INVALID_HANDLE)
		SetFailState("Your gameserveraccount_token.txt file is missing!");

	ReadFileString(hFileToken, sToken, maxlen);
	CloseHandle(hFileToken);

	PrintToServer("Logging into a steam server account, token: (%s) ...", sToken);
}
